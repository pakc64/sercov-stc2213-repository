package homeWork09.descendants;

import homeWork09.interfaces.Moveable;

public class Circle extends Ellipse implements Moveable {
    int perimeter;

    public Circle(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println("\nКРУГ");
        if (this.getX() == this.getY()) {                       //проверяем совподают ли координаты для построения круга
            perimeter = (int) (Math.PI * this.getX());          //так как я брал эти координаты как квадрат внутри
            System.out.println("Периметр круга = " + perimeter); //которого очерчен круг
        } else {
            System.out.println("Введины некоректные координаты...");
        }
    }

    @Override
    public void move(int x, int y) {   //в данном методе непонятки с кругом, в задании не сказано ни слово про радиус круга
        if (x == y) {                  // поэтому я взял поступающие координаты за границы в которых очерчен круг
            System.out.println("Перемещение круга на х = " + x + " y = " + y);
        } else {
            System.out.println("По новым координатам невозможно построить круг");
        }
    }
}
