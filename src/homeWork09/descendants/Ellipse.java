package homeWork09.descendants;

import homeWork09.abstractsClasses.Figure;

public class Ellipse extends Figure {
    int perimeter;

    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        perimeter = (int) (4 * (Math.PI * getX() * getY() + (getX() - getY())) / getX() + getY());  //вычесляем периметр
        System.out.println("\nЭЛИПС");
        System.out.println("Периметр элипса = " + perimeter);
    }

}
