package homeWork09.descendants;

import homeWork09.interfaces.Moveable;

public class Square extends Rectangle implements Moveable {
    int perimeter;

    public Square(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        System.out.println("\nКВАДРАТ");
        if (this.getX() == this.getY()) {
            perimeter = (this.getX() * 2 + this.getY() * 2);          //вычесляем периметр
            System.out.println("Периметр квадрата = " + perimeter);
        } else {
            System.out.println("Данные координаты не позволяют создать квадрат" +
                    " x = " + this.getX() + ", y = " + this.getY());
        }
    }

    @Override
    public void move(int x, int y) {                                           //метод показывает значение координат на
        System.out.println("Перемещение квадрата на х = " + x + " y = " + y);  //которое произошло смещение
        if (x != y) {
            System.out.println("По новым координатам невозможно построить квадрат");
        }
    }
}
