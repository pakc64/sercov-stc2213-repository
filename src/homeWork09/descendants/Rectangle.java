package homeWork09.descendants;

import homeWork09.abstractsClasses.Figure;

public class Rectangle extends Figure {
    int perimeter;

    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public void getPerimeter() {
        perimeter = (this.getX() * 2 + this.getY() * 2);          //вычесляем периметр
        System.out.println("\nПРЯМОУГОЛЬНИК");
        System.out.println("Периметр прямоугольника = " + perimeter);
    }
}
