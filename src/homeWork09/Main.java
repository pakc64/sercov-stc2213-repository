package homeWork09;

// Сделать абстрактный класс Figure, у данного класса есть два поля - x и y координаты.
// Классы Ellipse и Rectangle должны быть потомками класса Figure.
// Класс Square - потомок класса Rectangle, Circle - потомок класса Ellipse.
// В классе Figure предусмотреть абстрактный метод getPerimeter().
// Так же, нужно определить интерфейс Moveable c единственным методом .move(int x, int y), который позволит
// перемещать фигуру на заданные координаты.
// Данный интерфейс должны реализовать только классы Circle и Square.???????????????????????????????????????????????????
// В Main создать массив всех фигур и "перемещаемых" фигур. У всех вывести в консоль периметр, а у "перемещаемых"
// фигур изменить случайным образом координаты.

import homeWork09.abstractsClasses.Figure;
import homeWork09.descendants.Circle;
import homeWork09.descendants.Ellipse;
import homeWork09.descendants.Rectangle;
import homeWork09.descendants.Square;
import homeWork09.interfaces.Moveable;

public class Main {

    public static void main(String[] args) {

        Figure[] figures = new Figure[4];

        figures[0] = new Ellipse(4, 5);
        figures[1] = new Rectangle(4, 5);
        figures[2] = new Square(4, 4);
        figures[3] = new Circle(4, 4);


        for (int i = 0; i < figures.length; i++) {
            if (figures[i] instanceof Moveable) {      //проверка принадлежит ли обьект интерфейсу Moveable
                figures[i].getPerimeter();
                ((Moveable) figures[i]).move((int) (Math.random() * 50 + 1), (int) (Math.random() * 50 + 1));
            } else {
                figures[i].getPerimeter();
            }

        }
    }

}
