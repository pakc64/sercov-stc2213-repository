package homeWork09.interfaces;

public interface Moveable {
    public void move(int x, int y);
}
