package homeWork06;

import java.util.Random;

public class Task01 {
    public static void main(String[] args) {

      int randomNumber = randomLengthOfArray();
      int[] randomArray = fillArrayRandomNumber(-101, 100, randomNumber);
      findRarelyMetNumber(randomArray);
    }

    public static int randomLengthOfArray(){                            // Получение рандомного значения длинны массива
        Random random = new Random();
        int randomNumber = random.nextInt(100000) + 1;
        System.out.println("Длинна массива: " + randomNumber);
        return randomNumber;
    }

    public static int[] fillArrayRandomNumber(int min, int max, int lengthOfArray){             // Заполнение массива рандомными значениями (в диапазоне -100 : 100)
        int[] randomArray = new int[lengthOfArray];
        Random random = new Random();
        for(int i = 0; i < randomArray.length; i++) {
            int randomNumberForArray = random.nextInt(max - min + 1) + min;
            if(randomNumberForArray != -101) {
                System.out.print(randomNumberForArray + "  ");
                randomArray[i] = randomNumberForArray;
            } else {
                System.out.println();
                System.out.println(" Окончание ввода, Поступило значение -101.");
                break;
            }
        }
    return randomArray;
    }

    public static void findRarelyMetNumber(int[] randomArray) {
        int count = 0;
        int[] amountOfNumberArray = new int[202];
        for(int i = 0; i < randomArray.length; i++) {
            for(int j = i + 1; j < randomArray.length-1; j++) {
                if(randomArray[i] != 101){
                    if(randomArray[i] == randomArray[j]) {
                        randomArray[j] = 101;
                        count++;
                    }
                }
            }
            amountOfNumberArray[randomArray[i] + 100] = count;
            count = 0;
        }
        System.out.println("\n Минимальные значения в массиве связь по их индексу:");
        for(int i = 0; i < amountOfNumberArray.length; i++){
            System.out.print(amountOfNumberArray[i] + "  ");
       }
        System.out.println("\n\nЗначения появляющиеся в массиве минимальное количество раз:");
        for(int i = 0; i < amountOfNumberArray.length -1; i++) {
            if(amountOfNumberArray[i] < 1) {
                System.out.print(i -100 + ",  ");
            }
        }
    }
}
