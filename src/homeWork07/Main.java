package homeWork07;

import java.util.Random;

//   Создать класс Human используя инкапсуляцию (модификаторы и методы доступа), у которого будут поля:
//
//      String name
//      String lastName
//      int age
//
//      Будет два конструктора - один пустой, второй - полный (инициализирует все поля)
//
//      Создать класс Main, в котором будет создаваться массив на случайное количество человек.
//      После инициализации массива - заполнить его объектами класса Human (у каждого объекта случайное значение поля age).
//      После этого - отсортировать массив по возрасту и вывести результат в консоль

public class Main {
    public static void main(String[] args) {

        Human[] human = new Human[createLengthArrayOfPeople()];

        for(int i = 0; i < human.length; i++) {                      // заполнение массива обьектами 'human'
            human[i] = new Human("Human" + i, "HumanSurname" + i, createRandomAge());
            System.out.println(human[i]);
        }

        boolean isSorted = false;                                    // сортировка списка 'human' по позрастанию поля 'age'
        Human buffer;
        while (!isSorted) {
            isSorted = true;
            for(int i = 0; i <human.length -1; i++) {
                if(human[i].getAge() > human[i+1].getAge()) {
                    isSorted = false;
                    buffer = human[i];
                    human[i] = human[i+1];
                    human[i+1] = buffer;
                }
            }
        }

        System.out.println("Отсортированный массив");               //вывод отсортированного списка 'human'

        for(int i = 0; i < human.length; i++) {
            System.out.println(human[i]);
        }

    }

    public static int createLengthArrayOfPeople() {               // метод создает рандомное количество людей в массиве
        Random random = new Random();
        int arrayForRandomLength = random.nextInt(10 +1);
        System.out.println("Список состои из " + arrayForRandomLength + " человек");
        return arrayForRandomLength;
    }

    public static int createRandomAge(){                  //метод задает рандомное значение для поля 'age' обьекта Human
        Random random = new Random();
        int randomAge = random.nextInt(99 +1);
        return randomAge;
    }


}
