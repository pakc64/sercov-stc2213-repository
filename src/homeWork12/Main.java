package homeWork12;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        String string = "раз два три раз два два пять";
        String[] stringsWordsArr = string.split(" ");
        HashMap<String, Integer> map = fillingMap(stringsWordsArr);
        printMap(map);

    }

    private static void printMap(HashMap<String, Integer> map) {

        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            System.out.println(pair.getKey() + " - " + pair.getValue() + " повторения слова");
        }
    }

    public static LinkedHashMap<String, Integer> fillingMap(String[] wordsArr) {
        LinkedHashMap<String, Integer> map = new LinkedHashMap<>();

        for(String string : wordsArr) {
            if(map.containsKey(string)) {
                map.put(string, map.get(string) + 1);
            } else {
                map.put(string, 1);
            }
        }
        return map;
    }
}
