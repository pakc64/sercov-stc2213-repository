package firstAttestation.workingClasses;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class User {
  
    private int id;
    private String name;
    private String surname;
    private int age;
    private boolean job;

    public User() {
    }

    public User(int id, String name, String surname, int age, boolean job) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.job = job;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getJob() {
        return job;
    }

    public void setJob(boolean job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "User " + id +
                " " + name +
                " " + surname +
                " " + age +
                " " + job;
    }
}
