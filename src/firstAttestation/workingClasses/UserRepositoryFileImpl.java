package firstAttestation.workingClasses;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserRepositoryFileImpl {
    private List<User> userList;
    File file = new File("src/firstAttestation/TXTFiles/ListUsers.txt");

    public UserRepositoryFileImpl() {
        this.userList = new ArrayList<>();
        readFileUsers();
    }

    public void add(String name, String surname, int age, boolean gob) {
        User user = new User(createId() + 1, name, surname, age, gob);
        userList.add(user);
        writerFile();

    }

    public User findById(int id) {
        //userList.stream().filter(u -> u.getId() == id);
        for (User user : userList) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;
    }

    public void deleteById(int id) {
        userList.removeIf(user -> user.getId() == id);
        writerFile();
        userList.clear();
    }

    public void update(User user) {
        for (User userInList : userList) {
            if (userInList.getId() == user.getId()) {
                if ((userInList.getName()).equals(user.getName()))
                    userInList.setName(user.getName());
                if ((userInList.getSurname()).equals(user.getSurname()))
                    userInList.setSurname(user.getSurname());
                if (userInList.getAge() == user.getAge())
                    userInList.setAge(user.getAge());
                if ((userInList.getJob()) == user.getJob())
                    userInList.setJob(user.getJob());
            }
        }
        writerFile();
    }

    private int createId() {
        String s = "";
        try (BufferedReader reader = Files.newBufferedReader(
                Paths.get("src/firstAttestation/TXTFiles/ListUsers.txt"), StandardCharsets.UTF_8)) {
            List<String> line = reader.lines()
                    .skip(userList.size() - 1)
                    .collect(Collectors.toList());

            s = line.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String[] arrayData = s.split("\\|");
        int idInt = Integer.parseInt(arrayData[0]);

        return idInt;
    }

    private void createNewFile() {

        try {
            if (file.createNewFile())
                System.out.println("Создан новый файл по пути: " + file.getAbsolutePath());
        } catch (IOException e) {
            System.out.println("Произошла ошибка при создании или проверке файла");
        }
    }

    public void readFileUsers() {
        userList.clear();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] massUserData = line.split("\\|");
                User user = new User(Integer.parseInt(massUserData[0]),
                        massUserData[1],
                        massUserData[2],
                        Integer.parseInt(massUserData[3]),
                        Boolean.parseBoolean(massUserData[4]));

                userList.add(user);
            }

        } catch (FileNotFoundException e) {
            System.out.println("Файла " + file + " не существует");
            createNewFile();
        } catch (IOException e) {
            System.out.println("Произошла ошибка при чтении файла " + file);
            e.printStackTrace();
        }
//        for (int i = 0; i < userList.size(); i++) {
//            System.out.println(userList.get(i));
//        }
    }


    private void writerFile() {
        Writer writer = null;
        try {
            writer = new FileWriter(file, false);
            for (int i = 0; i < userList.size(); i++)
                writer.write(userList.get(i).getId() + "|"
                        + userList.get(i).getName() + "|"
                        + userList.get(i).getSurname() + "|"
                        + userList.get(i).getAge() + "|" + userList.get(i).getJob() + "\n");
            writer.flush();

        } catch (FileNotFoundException e) {
            System.out.println("Файл в который произойдет запись не существует!\n Создать? Да/Нет");

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String answer = reader.readLine();
                if (answer.equals("Да")) {
                    createNewFile();
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        } catch (IOException e) {
            System.out.println("Ошибка при записи файла");
        }

    }
}
