package firstAttestation;

import firstAttestation.workingClasses.User;
import firstAttestation.workingClasses.UserRepositoryFileImpl;

public class Main {
    public static void main(String[] args) {
        /**
         рабочие методы:
         .add(String name, String surname, int age, boolean job) - Добовляет в список нового User
         .deleteById(int) - Удаляет User из списка Users по id
         .findById(int) - Находить User в списке Users по id
         .update(User user) - Обновляет данные User
         **/
        UserRepositoryFileImpl user = new UserRepositoryFileImpl();

        user.add("Спанч", "Боб", 34, true);
        user.add("Патрик", "Звезда", 24, false);

//       System.out.println(user.findById(7));
//
//        user.deleteById(8);
//
//        User natalia = user.findById(6);
//        natalia.setAge(18);
//        user.update(natalia);
//        User ivan = user.findById(4);
//        ivan.setName("Петр");
//        user.update(ivan);
    }
}
