package homeWork05;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Task01 {

    public static void main(String[] args) throws IOException {
        int[] array = {4, 5, 7, 100, -4, 256, -1, 8, 345, 90, 11, 142, 3, 20, 54, 67, 16, 44, 32, 64};
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int number = Integer.parseInt(reader.readLine());
        System.out.println(findIndexInArray(array, number));


    }

    public static int findIndexInArray(int[] array, int number) {
        int index = -1;
        for(int i = 0; i < array.length; i++){
            if(array[i] == number) index = i;
        }
        return index;
    }
}
