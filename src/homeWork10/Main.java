package homeWork10;

//  Предусмотреть функциональный интерфейс:
//  interface ByCondition {
//    boolean isOk(int number); }

//    Написать класс Sequence, в котором должен присутствовать метод filter:
//    public static int[] filter(int[] array, ByCondition condition) {
//        ...
//        }
//        Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
//        В main в качестве condition подставить:
//       1) проверку на четность элемента
//       2) проверку, является ли сумма цифр элемента четным числом.
//       3) Доп. задача: проверка на четность всех цифр числа. (число состоит из цифр)
//       4) Доп. задача: проверка на палиндромность числа (число читается одинаково и слева, и справа -> 11 - палиндром,
//          12 - нет, 3333 - палиндром, 3211 - нет, и т.д.).

public class Main {
    public static void main(String[] args) {
        Sequence sequence = new Sequence();

        int[] array = new int[10];                                       // создание массива на 10 элементов
        System.out.println("Первоночальный массив:");
        for (int i = 0; i < array.length; i++) {                         // заполнение массива рандомными значениями от 0 до 640 включительно
            array[i] = (int) (Math.random() * 640 + 1);
            System.out.print(array[i] + " ");
        }

        System.out.println("\n--------------------------------------");

        System.out.println("Чётные числа:");

        sequence.filter(array, number -> number % 2 == 0);                 // лямда на четность

        System.out.println("Являються ли сумма цифр числа четными:");

        sequence.filter(array, number -> checkingForEvenSumDigits(number));  //лямда на определение четности суммы числа

        System.out.println("Четность всех цифр числа:");

        sequence.filter(array, number -> checkingAllEvenNumber(number));     // лямда на четность всех цифр числа

        System.out.println("Палидромность числа:");
        sequence.filter(array, number -> palindromeNumber(number));         // лямбда на палидромность


    }

    private static boolean checkingForEvenSumDigits(int number) {       // метод ко 2 лямбде
        int sum = 0;
        int bufferOfNumber = number;

        for (; bufferOfNumber > 0; bufferOfNumber /= 10) {           // сумируем цифры числа
            sum += bufferOfNumber % 10;
        }

        if (sum % 2 == 0) {                                        // проверяем на четность полученную сумму
            return true;
        }
        return false;

    }

    private static boolean checkingAllEvenNumber(int number) {    // метод к 3 лямде
        int bufferOfNumber = number;

        for (; bufferOfNumber > 0; bufferOfNumber /= 10) {        // определение четности всех чисел
            if (bufferOfNumber % 2 != 0) {
                return false;
            }
        }
        return true;
    }

    private static boolean palindromeNumber(int number) {         // метод к 4 лямбде
        int reversedNumber = 0;
        int bufferOfNumber = number;
        int digit;

        while (bufferOfNumber != 0) {
            digit = bufferOfNumber % 10;
            reversedNumber = reversedNumber * 10 + digit;
            bufferOfNumber /= 10;
        }
        return ((number == reversedNumber) ? true : false);
    }

}
