package homeWork10;

import homeWork10.interfaces.ByCondition;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int[] resultArray = new int[array.length];
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                System.out.print(array[i] + " ");
                resultArray[count] = array[i];
                count++;
            }
        }
        System.out.println("\n--------------------------------------");
        return resultArray;
    }
}
