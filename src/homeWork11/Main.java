package homeWork11;

//  Создать класс Human у которого будут поля:
//  private String name;
//  private String lastName;
//  private String patronymic;
//  private String city;
//  private String street;
//  private String house;
//  private String flat;
//  private String numberPassport;
//
//        Переопределить три метода: toString(), hashCode(), equals
//        Метод toString должен выводить информацию таким образом (пример):
//        Пупкин Вася Варфаламеевич
//        Паспорт:
//        Серия: 98 22 Номер: 897643
//        Город Джава, ул. Программистов, дом 15, квартира 54
//        Метод equals() должен сравнивать людей по номеру паспорта

public class Main {
    public static void main(String[] args) {

        Human constantin = new Human("Ткачев", "Константин", "Вячеславович", "Белгород",
                "Студенческая", "19", "64", "2022646464");

        Human elon = new Human("Илон", "Рив", "Маск", "Техас",
                "Бока-Чика", "1", "21", "001298765");

        Human spanchBob = new Human("Спанч", "Боб", "Сквепенс", "Бикини-Боттом",
                "Скотч Стрит", "124", "1", "001298765");

        System.out.println(constantin);
        System.out.println("совподение по паспорту 1: " + constantin.equals(elon));
        System.out.println("совподение по паспорту 2: " + spanchBob.equals(elon));

    }
}
