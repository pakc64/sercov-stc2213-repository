package homeWork08.descendants;

import homeWork08.Worker;

public class Devops extends Worker {
    public Devops(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {                                      // Метод в котором описана работа
        System.out.println("\n****Работает класс потомок****");
        System.out.println("" + getName() +" " + getLastName() + " - профессия: " + getProfession());
    }

    @Override
    public void goToVacation(int days) {                           // Метод в котором описан отпуск
        System.out.println(getName() + " " + getLastName() + "  - уходит в отпуск на " + days + " дней c 15 сентября");;
    }
}
