package homeWork08;

//Нужно создать класс Worker, с полями:
//
//        String name; // имя
//        String lastName; // фамилия
//        String profession; // профессия
//
//        Так же у класса Worker, есть два метода:
//
//public void goToWork(); //Метод в котором описана работа. Метод, как минимум, должен выводить кто работает, какая у
// него профессия и как он работает. Каждый потомок должен переопределить этот метод (каждая профессия работает по своему)
//public void goToVacation(int days); //Метод в котором описан отпуск. На вход принимает количество дней отпуска. Метод,
// как минимум, должен выводить кто уходит в отпуск, какая у него профессия и на сколько дней уходит в отпуск. Каждый
// потомок должен переопределить этот метод (каждая профессия отдыхает по своему)
//
//        Нужно создать минимум четыре класса потомка (профессию выбирайте сами:)), например - Программист, Тестировщик,
//        СисАдмин, ДевОпс и.т.д.
//        В классе Main (в котором создаются объекты всех классов профессий) через полиморфизм вывести отпуск и работу
//        каждого объекта по аналогии с "машинами" на уроке.

import homeWork08.descendants.Devops;
import homeWork08.descendants.Programmer;
import homeWork08.descendants.SysAdmin;
import homeWork08.descendants.Tester;

public class Main {
    public static void main(String[] args) {

        Worker[] workersArray = new Worker[5];
        workersArray[0] = new Worker();
        workersArray[1] = new Programmer("Иванов", "Иван", "Программист");
        workersArray[2] = new SysAdmin("Петров", "Петр", "SysAdmin");
        workersArray[3] = new Devops("Сидоров", "Игорь", "DevOps");
        workersArray[4] = new Tester("Козлов", " Нииколай", "Tester");

        int vacationDay = 30;                                //число отпускных дней

        for (int i = 0; i < workersArray.length; i++) {
            workersArray[i].goToWork();                      //вызов метода работы
            workersArray[i].goToVacation(vacationDay);       // вызов метода отпуска
            vacationDay-=5;                                  // уменьшение отмуска на 5 дней для последующих рабочих
        }

    }
}
