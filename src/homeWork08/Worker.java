package homeWork08;

public class Worker {
    private String name;
    private String lastName;
    private String profession;

    public Worker() {}

    public Worker(String name, String lastName, String profession) {
        this.name = name;
        this.lastName = lastName;
        this.profession = profession;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getProfession() {
        return profession;
    }

    public void goToWork(){                                    // Метод в котором описана работа
        System.out.println("\n****Работает родительский класс!****");
        System.out.println("Все работают!!!");
    }

    public void goToVacation(int days) {                       // Метод в котором описан отпуск
        System.out.println("Никаких тебе " + days + " отпускных дней. Батрачим все 365 дней!!!!");
    }
}


